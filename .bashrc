#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export GPG_TTY=$(tty)

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '


alias config='/usr/bin/git --git-dir=/home/afontana/.dotfiles/ --work-tree=/home/afontana'
alias start-ssh-agent='/usr/bin/ssh-agent -s >~/.ssh-agent-environment; . ~/.ssh-agent-environment'


[[ -s ~/.ssh-agent-environment ]] && . ~/.ssh-agent-environment &>/dev/null


